"""
Model exported as python.
Name : HW_Generator
Group : 
With QGIS : 33203
"""

from qgis.core import QgsProcessing
from qgis.core import QgsProcessingAlgorithm
from qgis.core import QgsProcessingMultiStepFeedback
from qgis.core import QgsProcessingParameterEnum
from qgis.core import QgsProcessingParameterNumber, QgsProcessingParameterFile, QgsProcessingParameterString
import processing
import math
import numpy as np
import pandas as pd
from pathlib import Path
import typing
import os


class Hw_sediment(QgsProcessingAlgorithm):

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterString('shape_id', 'Shape ID', multiLine=False, defaultValue=''))
        self.addParameter(
            QgsProcessingParameterEnum('shape', 'Shape', options=['Circular', 'Rectangular', 'Arch', 'Sprung Arch'],
                                       allowMultiple=False,
                                       usesStaticStrings=False, defaultValue=[0]))
        self.addParameter(
            QgsProcessingParameterNumber('width_or_dia_m', 'Width_or_Dia (m)', type=QgsProcessingParameterNumber.Double,
                                         minValue=0, defaultValue=None))
        self.addParameter(
            QgsProcessingParameterNumber('height_or_wf_for_rectangle_shapem', 'Height_or_WF (for Rectangular Shape)(m)',
                                         optional=True, type=QgsProcessingParameterNumber.Double, minValue=0,
                                         defaultValue=None))
        self.addParameter(
            QgsProcessingParameterNumber('crown_height', 'Crown Height (for Arch and Sprung Arch Shape)(m)',
                                         optional=True, type=QgsProcessingParameterNumber.Double, minValue=0,
                                         defaultValue=None))
        self.addParameter(
            QgsProcessingParameterNumber('springing_height', 'Springing Height (for Sprung Arch Shape)(m)',
                                         optional=True, type=QgsProcessingParameterNumber.Double, minValue=0,
                                         defaultValue=None))
        self.addParameter(QgsProcessingParameterNumber('sediment_depth_mm', 'Sediment Depth (mm)', optional=True,
                                                       type=QgsProcessingParameterNumber.Double, defaultValue=0))
        self.addParameter(
            QgsProcessingParameterNumber('number_of_heightwidth_entries', 'Number of Height/Width Entries',
                                         type=QgsProcessingParameterNumber.Integer, minValue=10, maxValue=50,
                                         defaultValue=10))
        self.addParameter(QgsProcessingParameterFile('output_location', 'Output Directory',
                                                     behavior=QgsProcessingParameterFile.Folder,
                                                     fileFilter='All files (*.csv*)', defaultValue=None))

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(0, model_feedback)
        feedback.setCurrentStep(0)
        results = {}
        outputs = {}

        # Parameters

        shape_ID = parameters['shape_id']
        shape = parameters['shape']
        width = parameters['width_or_dia_m']
        rect_height = parameters['height_or_wf_for_rectangle_shapem']
        diameter = parameters['width_or_dia_m']
        num_steps = parameters['number_of_heightwidth_entries']
        crown_height = parameters['crown_height']
        springing_height = parameters['springing_height']
        sediment_depth = parameters['sediment_depth_mm']
        if sediment_depth:
            sediment_depth = sediment_depth / 1000
        output_location = parameters['output_location']
        output = f'{output_location}\\1D_HW_{shape_ID}.csv'
        output_ = Path(output)

        # Function to Calculate Widths of Circles at different heights
        def calculate_circle_width(diameter: float):
            """
            Create circle widths for given height.

            :param diameter: float
            :return: widths
            """
            radius = diameter / 2
            widths = []
            for height in heights:
                if abs(height) <= radius:
                    height = radius - height
                    width = (2 * math.sqrt(radius ** 2 - height ** 2))
                    widths.append(width)
                elif abs(height) > radius:
                    height = height - radius
                    width = (2 * math.sqrt(radius ** 2 - height ** 2))
                    widths.append(width)
                else:
                    widths.append('error')  # Or any other value to indicate an invalid height
            return widths

        # Function to Calculate Widths of Arches and Sprung Arches at different heights
        def parabolic_arch_conduit(width: float, crown_height: float, springing_height: float):
            """
            Create parabolic arch.

            Assumes that springing height is the same at both ends.  Uses width and crown height for arches, and width,
            crown height, springing height for sprung arches.

            :param width: float
            :param crown_height: float
            :param springing_height: float (optional for arch but required for sprung arch)
            :return: widths
            """
            widths = []
            a = -crown_height / ((-width / 2) ** 2)
            if springing_height > 0:
                if crown_height <= width / 2:
                    for height in heights:
                        if height <= springing_height:
                            widths.append(width)
                        else:
                            if height - springing_height < crown_height:
                                height = height - springing_height
                                width = 2 * (math.sqrt((height - crown_height) / a))
                                widths.append(width)
                            else:
                                width = 0
                                widths.append(width)
                else:
                    print("The crown height should be less than half of the width.")
            else:
                if crown_height <= width / 2:
                    for height in heights:
                        if height < crown_height:
                            height = height - springing_height
                            width = 2 * (math.sqrt((height - crown_height) / a))
                            widths.append(width)
                        else:
                            width = 0
                            widths.append(width)
                else:
                    print("The crown height should be less than half of the width.")

            return widths

        # Function to Calculate Widths of Rectangles at different heights
        def calculate_rect_widths(width: float, rect_height: float):
            """
            Create rectangular widths for given heights.  The width is used up the rectangle height and then an
            additional entry is added to 'close the HW table'.

            :param width: float
            :param rect_height: float
            :return: widths
            """
            widths = []
            for height in heights:
                if height > rect_height:
                    width = 0
                    widths.append(width)
                else:
                    width = width
                    widths.append(width)
            return widths

        # Calculate Height Array
        if shape == 0:  # Circular Conduit
            if sediment_depth < diameter:
                heights = np.linspace(0 + sediment_depth, diameter, num_steps)
                widths = calculate_circle_width(diameter)  # Calculate widths
            else:
                feedback.reportError('Error: Sediment Depth is greater than conduit Width_or_Dia (m)')
        elif shape == 1:  # Rectangular
            if rect_height:
                if sediment_depth < rect_height:
                    heights = np.linspace(0 + sediment_depth, rect_height, num_steps)
                    heights = np.append(heights, rect_height + 0.01)
                    widths = calculate_rect_widths(width, rect_height)  # Calculate widths
                else:
                    feedback.reportError(
                        'Error: Sediment Depth is greater than conduit Height_or_WF (for Rectangle Shape)(m)')
            else:
                feedback.reportError(
                    'Error: Height_or_WF (for Rectangle Shapes)(m) is a required parameter for rectangular shapes')
        elif shape == 2:  # Arch
            if crown_height:
                if sediment_depth < crown_height:
                    heights = np.linspace(0 + sediment_depth, crown_height, num_steps)
                    widths = parabolic_arch_conduit(width, crown_height, 0)  # Calculate widths
                else:
                    feedback.reportError('Error: Sediment Depth is greater than conduit crown height')
            else:
                feedback.reportError(
                    'Error: Crown Height (for Arch and Sprung Arch Shapes)(m) is a required parameter for Arch Shapes')
        elif shape == 3:  # Sprung Arch
            if crown_height:
                if springing_height:
                    if sediment_depth < springing_height:
                        rect_heights = [0 + sediment_depth, springing_height]
                        arch_heights = np.linspace(springing_height, springing_height + crown_height, num_steps)
                        heights = np.append(rect_heights, arch_heights)
                        widths = parabolic_arch_conduit(width, crown_height, springing_height)  # Calculate widths
                    else:
                        feedback.reportError('Error: Sediment Depth is greater than conduit springing Height')
                else:
                    feedback.reportError(
                        'Error: Springing Height (for Sprung Arch Shapes)(m) is a required parameter for Sprung Arch shapes')
            else:
                feedback.reportError(
                    'Error: Crown Height (for Arch and Sprung Arch Shapes)(m) is a required parameter for Sprung Arch shapes')
        # Results to csv
        df = pd.DataFrame(
            {'Height': [round(height, 3) for height in heights], 'Width': [round(width, 3) for width in widths]})

        # Remove rows where 'Height' value is less than the sediment depth
        # filtered_df = df[df['Height'] >= sediment_depth]
        df.to_csv(f'{output_}', index=False)
        feedback.pushInfo('Writing TUFLOW HW File...')
        feedback.setCurrentStep(1)

        for height, width in zip(heights, widths):
            print(f"For height {height:.2f}, the corresponding width is {width:.2f}")

        return results

    def name(self):
        return 'HW_Generator'

    def displayName(self):
        return 'HW_Generator'

    def shortDescription(self) -> typing.AnyStr:
        return self.tr("Generate TUFLOW HW Tables and optionally apply a sediment depth")

    def shortHelpString(self) -> typing.AnyStr:
        folder = Path(os.path.realpath(__file__)).parent
        help_filename = folder / 'help' / 'html' / 'HW_Generator.html'
        return help_filename.open().read()

    def group(self):
        return ''

    def groupId(self):
        return ''

    def createInstance(self):
        return Hw_sediment()
