### Description

This QGIS processing tool is used to generate TUFLOW Height Width Tables for use with Irregular Culverts specified in the 1D_Nwk File.

The tool allows the user to specify a Circular or Rectangular Culvert and the tool will generate Height-Width tables for these.  
The tool is set up so that the user can specify an optional sediment depth, and the tool will adjust the Height-Width table accordingly.  
The change in invert level, as a consequence of the sediment depth, will need to be added to the invert level in the 1D_nwk input layer.

### Setup

Download the files, unzip and put in the QGIS processing scripts folder usually something like C:\Users\USERNAME\AppData\Roaming\QGIS\QGIS3\profiles\default\processing\scripts.  Then open QGIS and you should see a tool in the Processing toolboxes.  

### Parameters

* **Shape ID**: Unique Identifier for the Shape. [Required]
* **Shape**: Culvert Shape, currently Circular or Rectangular. [Required]
* **Width_or_Dia (m)**: As per the TUFLOW 1D_nwk attribute, the pipe diameter in metres for circular shaped culverts or the box culvert width in metres for rectagular culverts. [Required]
* **Height_or_WF (for Rectangular Shape)(m)**: As per the TUFLOW 1D_nwk attribute, the box culvert height in metres for rectagular culverts. [Required for Rectangular Culverts]
* **Crown Height (for Arch and Sprung-Arch Shape)(m)**: This is the height of the Arch culverts. For Sprung-Arch Culverts it is the arch height above the springing height [Required for Arch/Sprung-Arch Culverts]
* **Springing Height (for Sprung-Arch Shape)(m)**: This is the springing height of the Sprung-Arch culverts. [Required for Sprung-Arch Culverts]
* **Sediment Depth (mm)**: An optional user specified sediment depth (mm) to apply to the height-width table.  Note the mm units.
* **Number of Height/Width Entries**: Number of Height/Width pairs used to define the shape in the HW table.  Default and minimum is 10.
* **Output Directory**: Location to which the TUFLOW HW.csv is to be saved to.
