### Description

This tool is used to generate TUFLOW Height Width Tables for use with Irregular Culverts specified in the 1D_Nwk File.

The tool allows the user to specify a Circular or Rectangular Culvert and the tool will generate Height-Width tables for these.  
The tool is set up so that the user can specify an optional sediment depth, and the tool will adjust the Height-Width table accordingly.  
The change in invert level, as a consequnece of the sediment depth, will need to be added to the invert level in the 1D_nwk input layer.  

### Parameters

* **Shape**: Culvert Shape, currently Circular or Rectangular. [Required]
* **Width_or_Dia (m)**: As per the TUFLOW 1D_nwk attribute, the pipe diameter in metres for circular shaped culverts or the box culvert width in metres for rectagular culverts. [Required]
* **Height_or_WF (for Rectangular Shape)(m)**: As per the TUFLOW 1D_nwk attribute, the box culvert height in metres for rectagular culverts. [Required for Rectangular Culverts]
* **Sediment Depth (mm)**: An optional user specified sediment depth (mm) to apply to the height-width table.  Note the mm units.
* **Number of Height/Width Entries**: Number of Height/Width pairs used to define the shape in the HW table.  Default and minimum is 10.
* **Output Directory**: Location to which the TUFLOW HW.csv is to be saved to.
